package com.company.anotherproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class AnotherProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnotherProjectApplication.class, args);
    }
}
