module "another-project_db" {
  source = "./db"

  name                                  = var.another-project_db_name
  engine                                = var.another-project_db_engine
  engine_version                        = var.another-project_db_engine_version
  instance_class                        = var.another-project_db_instance_class
  storage                               = var.another-project_db_storage
  user                                  = var.another-project_db_user
  password                              = var.another-project_db_password
  random_password                       = var.another-project_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.another-project_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
